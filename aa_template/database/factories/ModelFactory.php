<?php

  $factory->define(App\Models\AA_TEST_SAMPLE\Aa_brid::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AA_TEST_SAMPLE\Aa_dogs::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\AA_TEST_SAMPLE\Aa_dog::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AA_TEST_SAMPLE\Aa_bird::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AB_TEST_SAMPLE\Aa_bird::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AC_TEST_SAMPLE\Aa_bird::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AB_TEST_SAMPLE\Ab_bird::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AC_TEST_SAMPLE\Ac_bird::class, function (Faker\Generator $faker) {
    return [
    ];
});

